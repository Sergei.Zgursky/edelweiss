$('.advantages__li').each(function(){
    $(this).find('.advantages__li-box').append($(this).find('.advantages__li-icon-box').clone().removeClass('advantages__li-icon-box').addClass('advantages__li-bg-box'));
    $(this).find('.advantages__li-bg-box .advantages__li-icon').removeClass('advantages__li-icon').addClass('advantages__li-bg')
    var width = $(this).find('.advantages__li-bg').attr('width') * 10,
        height = $(this).find('.advantages__li-bg').attr('height') * 10;
    $(this).find('.advantages__li-bg').attr('width', width);
    $(this).find('.advantages__li-bg').attr('height', height);
})
$(document).on('click', '.js-advertisement-close', function(){
    $(this).closest('.advertisement').slideUp().addClass('hide1');

    return false
})

$(document).on('click', '.amount__bt-plus', function(){
    var amountSum = $(this).closest('.amount').attr('data-amountSum'),
        amountInput = $(this).siblings('.amount__input').val();
    amountSum = JSON.parse(amountSum);
    var amountSumSize = amountSum.length;
    amountSum.indexOf(+amountInput);
    if(amountSum.indexOf(+amountInput) + 1 < amountSumSize) {
        $(this).siblings('.amount__input').val(amountSum[amountSum.indexOf(+amountInput) + 1])
    }
    if($(this).closest('.amount').hasClass('configurator__amount')){
        var sum = $(this).siblings('.amount__input').val();
        $(this).closest('.amount').siblings('.configurator__equipment-radio').find('.configurator__equipment-radio-input').attr('data-sum', sum)
        result($(this).closest('.configurator__equipment-list-box').find('.configurator__equipment-radio-input'));
        counterCall()
    }
    return false
});
$(document).on('click', '.amount__bt-minus', function(){
    var amountSum = $(this).closest('.amount').attr('data-amountSum'),
        amountInput = $(this).siblings('.amount__input').val();
    amountSum = JSON.parse(amountSum);
    var amountSumSize = amountSum.length;
    amountSum.indexOf(+amountInput);

    if(amountSum.indexOf(+amountInput) > 0) {
        $(this).siblings('.amount__input').val(amountSum[amountSum.indexOf(+amountInput) - 1])
    }
    if($(this).closest('.amount').hasClass('configurator__amount')){
        var sum = $(this).siblings('.amount__input').val();
        $(this).closest('.amount').siblings('.configurator__equipment-radio').find('.configurator__equipment-radio-input').attr('data-sum', sum);
        result($(this).closest('.configurator__equipment-list-box').find('.configurator__equipment-radio-input'));
        counterCall()
    }
    return false
});

$(document).on('click', '.amount__bt-unit-plus', function(){
    var inp = $(this).siblings('input:text');
    var curNumbInp = parseFloat(inp.val());
    inp.val(parseFloat(++curNumbInp));
    return false;
});
$(document).on('click', '.amount__bt-unit-minus', function(){
    var inp = $(this).siblings('input:text');
    var curNumbInp = parseFloat(inp.val());
    if(curNumbInp > 1){
        inp.val(parseFloat(--curNumbInp));
    }
    return false;
});
function $tile($el) {
    if (!$el.length) return;
    $el.masonry({
        itemSelector: 'li',
        singleMode: true,
        isResizable: true,
        isAnimated: true,
        horizontalOrder: true,
        animationOptions: { 
            queue: false, 
            duration: 500 
        }
    });
};
$('.article-list__container').each(function(){
    var $masonry = $(this);
    var update = function(){
        $masonry.masonry();
    };
    $('.article-list__li', $masonry);
    $masonry.masonry();
    this.addEventListener('load', update, true);
});
var userAgent = navigator.userAgent.toLowerCase();

var ff = /firefox/.test(userAgent);

var mac = /mac/.test(userAgent);
if(mac == true){
    $('body').addClass('macos')
}
if($('.basket__input-massage').length){
    $('.basket__input-massage').clone().addClass('basket__input-massage--mobile').prependTo($('.basket__order-box-btn'))
}
if($('.basket').length){
    $('.footer').addClass('footer--padding-mob')
    $('.basket').append('<div class="basket__control"/>');
    $('.basket__control').append($('.basket__worth').clone(), $('.basket__order-btn').clone());
}   
$(document).on('click', '.catalog__credit a', function(){
    $('.catalog__credit').removeClass('_active');
    $(this).closest('.catalog__credit').toggleClass('_active');
    return false
})

$(document).on('click', function(event){
    if( $(event.target).closest('.catalog__credit').length) 
    return; 
    $('.catalog__credit').removeClass('_active');   
    event.stopPropagation();
});

function positionHidd(){
    $('.catalog__credit').each(function(){
        var windowWidth = $(window).width();
            positionCredit = $(this).offset().left
    
        if(windowWidth-positionCredit <= 150) {
            $(this).addClass('catalog__credit--left')
        }else {
            $(this).removeClass('catalog__credit--left')
        }

    })
}
if($('.catalog__credit').length) {
    $(document).on('DOMContentLoaded', function(){
        positionHidd()
    })
    $(window).on('resize', function(){
        setTimeout(function(){
            positionHidd()
        }, 300)
    })
}
$('[data-cloneMobile]').each(function(){ 
     $('.'+$(this).attr('data-cloneMobile')).after($(this).clone().addClass('clone-mobile'));
});
$(document).on('click', '.configurator__result-list-name', function(){
    var id  = $(this).attr('href'),
        top = $('[data-id="'+id+'"]').offset().top - 80;
    $('body,html').animate({scrollTop: top}, 500);
    return false
});

$('.select__configurator').on('select2:select', function (e) {
    var linkItem = $(this.selectedOptions[0]).data('load'),
        $this = $(this);
    if(linkItem != 'none'){
        var ajaxPromise = new Promise(function(resolve, reject){
            $.ajax({
                url: linkItem,
                success: function(data) {
                    $this.closest('.configurator__equipment-item').find('.configurator__equipment-box').empty().append(data);
                    $('.configurator__result-box').addClass('_active')
                    $('.footer').addClass('footer--padding-mob')
                    resolve()
                },
                error: function(){
                }
            });
        });
        ajaxPromise.then(function(){
            $this.closest('.configurator__equipment-item').find('.configurator__equipment-list').slideDown()

            //зачекать первый товар в группе 
            // $this.closest('.configurator__equipment-item').find('.configurator__equipment-radio-input:eq(0)').prop('checked', true).closest('.configurator__equipment-list-box').addClass('_active')
            // pricePlus($this.closest('.configurator__equipment-item').find('.configurator__equipment-radio-input:eq(0)'))
            // var id = $this.closest('.configurator__equipment-item').data('id');
            // $('a[href="'+id+'"]').closest('.configurator__title-box').addClass('_active')
            
            // var attrImg = $('.configurator__equipment-radio-input').attr('data-case');

            // if (typeof attrImg !== typeof undefined && attrImg !== false) {
            //     $('.configurator__result-img').attr('src', attrImg)
            //     $('.configurator__result-img-box').addClass('_active')
            // }
            
            // result($this.closest('.configurator__equipment-item').find('.configurator__equipment-radio-input:eq(0)'))

            // //запуск счетчика 
            // counterCall()
        })
        
    }else {
        
        // var id = $this.closest('.configurator__equipment-item').data('id');
        // $('a[href="'+id+'"]').closest('.configurator__title-box').removeClass('_active')
        // $this.closest('.configurator__equipment-item').removeClass('_active').find('.configurator__equipment-box').empty()
        // $('.configurator__result-box').removeClass('_active')
        // counterCall()
    }
    $(this).closest('.configurator__equipment-list-box').find('.configurator__equipment-radio-price').remove();
});

$(document).on('change', '.configurator__equipment-radio-input', function(){
    $(this).closest('.configurator__equipment-item').addClass('_active')
    $(this).closest('.configurator__equipment-list-box').addClass('_active').siblings('.configurator__equipment-list-box').removeClass('_active')
    pricePlus($(this))
    $(this).closest('.configurator__equipment-list').find('.configurator__equipment-img').attr('src', $(this).data('img'))
    $(this).closest('.configurator__equipment-list-box').find('.configurator__equipment-radio-price').remove();
    counterCall()

    var attrImg = $(this).attr('data-case');
    
    if (typeof attrImg !== typeof undefined && attrImg !== false) {
        $('.configurator__result-img').attr('src', attrImg)
        $('.configurator__result-img-box').addClass('_active')
    }

    result($(this))

});

//добавление к сборке
function result($el){
    var resultId = $el.closest('.configurator__equipment-item').data('id'),
        resultCont = $('.configurator__result-list-name[href="'+resultId+'"]').closest('.configurator__result-list-box'),
        resultMobileName = $el.closest('.configurator__equipment-item').find('.configurator__equipment-name-select'),
        resultText;


    if(!$el.closest('.configurator__equipment-arr').length){
        resultText = $el.siblings('.configurator__equipment-radio-text').text();
    }else {
        let arr = [];
        $el.closest('.configurator__equipment-arr').find('input:checked').each(function(){
            if($(this).attr('data-price') != 0) {
                arr.push($(this).siblings('.configurator__equipment-radio-text').text())
            }
        });
        resultText = arr.join()
    }


        resultCont.find('.sum-box').remove();
    if($el.closest('.configurator__equipment-list-box').find('.amount').length){
        var sum = $el.closest('.configurator__equipment-list-box').find('.amount').find('.amount__input').val();
        if(sum != 1){
            resultCont.find('.configurator__result-list-current').addClass('_sum')
            .after('<p class="sum-box">x'+sum+'</p>')
        }
    }
    if($el.data('price') != 0||$el.closest('.configurator__equipment-arr').length){
        if(!resultCont.hasClass('_active')){
            resultCont.addClass('_active')
        }
        resultCont.find('.configurator__result-list-current').text(resultText);
        resultMobileName.text(resultText);
    }else{
        resultCont.removeClass('_active')
    };
    if($('.configurator__result-list').find('._active').length>4){
        $('.configurator__result-list').addClass('_hide')
    }else if($('.configurator__result-list').hasClass('_hide')){
        $('.configurator__result-list').removeClass('_hide')
    }
}
//Запуск счетчика цены
function counterCall(){
    var currentPrice = $('.configurator__price-rez:eq(1)').text();
    currentPrice = currentPrice.replace(/\s/g, '');
    if(currentPrice == Number.NaN||currentPrice == 'нечисло' ||currentPrice == 'не число'){
        currentPrice = 0
    }
    var priceEl = 0;
    $('.configurator__equipment-radio-input:checked').each(function(){
        priceEl = $(this).data('price') + priceEl;
        var attrSum = $(this).attr('data-sum');

        if (typeof attrSum !== typeof undefined && attrSum !== false) {
            priceEl = priceEl * $(this).attr('data-sum');
        }
    })
    $('.configurator__price-credit').addClass('_active');
    $('.configurator__result-btn-box').addClass('_active');
    $('.configurator__price-rez').each(function(){
        counter($(this), currentPrice, priceEl);
    })
    $('.configurator__price-credit-sum').each(function(){
        counter($(this), currentPrice/10, priceEl/10);
    })
}

//Настройки счетчика цены
function counter($el, current, price) {
    $el.countTo({
        from: +current, 
        to: price,
        speed: 500,
        refreshInterval: 50,
        formatter: function (value, options) {
            return value.toLocaleString('ru-RU');
        }
    });
}

$(document).on('click', '.configurator__result-btn-open', function(){
    $('.configurator__title').slideToggle()
    $(this).toggleClass('_open')
    var open = $(this).data('open'),
        close = $(this).data('close'),
        heightBox = $('.configurator__result-list-content').height();
    if($(this).hasClass('_open')){
        $('.configurator__result-list').css('max-height', heightBox)
        $(this).text(close)
    }else {
        $('.configurator__result-list').removeAttr('style')
        $(this).text(open)
    }
    return false
})

$(document).on('click', '.configurator__result-btn-open-mob', function(){
    $(this).toggleClass('_open')
    var open = $(this).data('open'),
        close = $(this).data('close'),
        heightBox = $('.configurator__result-list-content').height();
    if($(this).hasClass('_open')){
        $('.configurator__result-list').css('max-height', heightBox)
        $(this).text(close)
        
        $('.configurator__equipment-item').addClass('_open')
    }else {
        $('.configurator__result-list').removeAttr('style')
        $('.configurator__equipment-item').removeClass('_open')
        $(this).text(open)
    }
    return false
})

//+- к цене 
function pricePlus($el) {
    var price = $el.data('price');
    $el.closest('.configurator__equipment-list-box').siblings('.configurator__equipment-list-box').each(function(){
        var priceRez = $(this).find('.configurator__equipment-radio-input').data('price');
        $(this).find('.configurator__equipment-radio-price').remove();
        priceRez = priceRez - price
        if(priceRez > 0) {
            $(this).find('.configurator__equipment-radio-text').append('<span class="configurator__equipment-radio-price">+'+priceRez+'руб.</span>')
        }else if(priceRez < 0) {
            $(this).find('.configurator__equipment-radio-text').append('<span class="configurator__equipment-radio-price">'+priceRez+'руб.</span>')
        }
    })
}

// счетчик
(function ($) {
    $.fn.countTo = function (options) {
        options = options || {};

        return $(this).each(function () {
            var settings = $.extend({}, $.fn.countTo.defaults, {
                from:            $(this).data('pricefrom'),
                to:              $(this).data('priceto'),
                speed:           $(this).data('speed'),
                refreshInterval: $(this).data('refresh-interval'),
                decimals:        $(this).data('decimals')
            }, options);

            var loops = Math.ceil(settings.speed / settings.refreshInterval),
                increment = (settings.to - settings.from) / loops;

            var self = this,
                $self = $(this),
                loopCount = 0,
                value = settings.from,
                data = $self.data('countTo') || {};

            $self.data('countTo', data);

            if (data.interval) {
                clearInterval(data.interval);
            }
            data.interval = setInterval(updateTimer, settings.refreshInterval);
            render(value);

            function updateTimer() {
                value += increment;
                loopCount++;

                render(value);

                if (typeof(settings.onUpdate) == 'function') {
                    settings.onUpdate.call(self, value);
                }

                if (loopCount >= loops) {
                    $self.removeData('countTo');
                    clearInterval(data.interval);
                    value = settings.to;

                    if (typeof(settings.onComplete) == 'function') {
                        settings.onComplete.call(self, value);
                    }
                }
            }

            function render(value) {
                var formattedValue = settings.formatter.call(self, value, settings);
                $self.html(formattedValue);
            }
        });
    };

    $.fn.countTo.defaults = {
        from: 0,               
        to: 0,                 
        speed: 1000,           
        refreshInterval: 100,  
        decimals: 0,           
        formatter: formatter, 
        onUpdate: null,        
        onComplete: null       
    };

    function formatter(value, settings) {
        return value.toFixed(settings.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ' ');
    }
}(jQuery));

$(document).on('click', '.js-configurator__control-btn', function(){
    $(this).siblings('.configurator__control-btn').toggleClass('_open');
    $('body').toggleClass('bgActive')
    return false
})
$(document).on('click', '.bgDark, .configurator__control-btn', function(event){
    if($(event.target).closest('.btn').length) 
    return; 
    $('.configurator__control-btn').removeClass('_open');   
    $('body').removeClass('bgActive');
    event.stopPropagation();
});
$('.configurator__result-btn--3').clone().addClass('mob').appendTo($('.configurator__control-btn'));
$('.configurator__result-btn--2').clone().addClass('mob').appendTo($('.configurator__control-btn'));

if($('.configurator__result-box').length){
    $('.configurator__result-box').before('<div class="configurator__result-mobile"><div class="configurator__result-mobile-start"/><div class="configurator__result-mobile-end"/></div>')
    $('.configurator__result-mobile-start').append($('.configurator__number').clone())
    $('.configurator__result-mobile-start').append($('.configurator__price-box').clone())
    $('.configurator__result-mobile-end').append($('.configurator__result-img-box').clone())
}
$(document).on('click', '.configurator__equipment-name', function(){
    $(this).closest('.configurator__equipment-item').toggleClass('_open')
    $('.configurator__result-btn-open-mob').each(function(){
        if($('.configurator__equipment-item').hasClass('_open')){
            $(this).addClass('_open')
            var close = $(this).data('close');
            $(this).text(close)
            }else {
                $(this).removeClass('_open')
                var open = $(this).data('open');
                $(this).text(open)
            }
    })

    return false
})
var detalicScroll,
    detalicSalePosition;
var detalicP = function(){
    detalicScroll = $(document).scrollTop();
    detalicSalePosition = $('.catalog').offset().top - $(window).height();
    if(detalicScroll >= detalicSalePosition){
        $('.detalic__control').addClass('hide')
    }else {
        $('.detalic__control').removeClass('hide')
    }
}
if($('.detalic__control').length && $('.catalog').length) {
    $(document).on('DOMContentLoaded', function(){
        detalicP()
    })
    $(window).on('resize', function(){
        setTimeout(function(){
            detalicP()
        }, 100)
    })
    $(document).on('scroll load', function(){
        detalicP()
    })
}
$(document).on('click', '.faq-page__content-question', function(){
    $(this).siblings('.faq-page__content-answer').slideToggle();
    $(this).closest('li').toggleClass('_active');
    return false
})
if($('.basket').length||$('.detalic').length || $('.fps').length){
    $('.footer').addClass('footer--padding-mob')
}
$(document).on('click', '.to-top', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
});
var headerPosition;
var fHeader = function(){
    headerPosition = $('.header__foot').offset().top,
    $('.header').css('--top', $('.header__foot').height()); 
}
$(document).on('DOMContentLoaded', function(){
    fHeader()
})
$(window).on('resize', function(){
    setTimeout(function(){
        fHeader()
    }, 100)
})
$(document).on('scroll load', function(){
    if($(this).scrollTop() >= headerPosition) {
       if(!$('.header').hasClass('_fix').lenght) {
           $('.header').addClass('_fix'); 
       }
    }else {
        $('.header').removeClass('_fix'); 
    }
})

var header = $('.header'),
    scrollPrev = 0,
    scrollMarg = 100,
    hheaderOffser;

var hheaderOffserFun = function(){
    hheaderOffser = header.offset().top;
}
$(document).on('DOMContentLoaded', function(){
    hheaderOffserFun()
})
$(window).on('resize', function(){
    setTimeout(function(){
        hheaderOffserFun()
    }, 300)
})
$(window).scroll(function() {
    var advertisementHeight = $('.advertisement').innerHeight();
    var scrolled = $(window).scrollTop();
    if(hheaderOffser < scrolled) {
        $('.header__head').addClass('_fix-mob')
    }else {
        $('.header__head').removeClass('_fix-mob')
    }
	if ( scrolled > scrollMarg + hheaderOffser && scrolled > scrollPrev ) {
		header.addClass('header__out');
	} else {
		header.removeClass('header__out');
	}
	scrollPrev = scrolled;
});
$(document).on('click', '.js-anhor', function(){
    var id  = $(this).attr('href'),
        top = $('[data-id="'+id+'"]').offset().top - 80;
        
    $('body,html').animate({scrollTop: top}, 500);
    return false
});
$(document).on('click', '.js-control-btn', function(){
    $(this).closest('.detalic__control').toggleClass('_open');
    $('body').toggleClass('bgActive')
    return false
})

if($('.js-control-btn').length && $('.js-control-btn').length) {
    $('.detalic__control').addClass('mob-bt');
}
$(document).on('click', '.bgDark, .detalic__control-btn-box', function(event){
    if($(event.target).closest('.detalic__control-btn-2').length) 
    return; 
    $('.detalic__control').removeClass('_open');   
    $('body').removeClass('bgActive');
    event.stopPropagation();
});
$(document).on('click', '.js-fps-btn', function(){
    $(this).toggleClass('_active');    
    $('.js-search-btn').removeClass('_active')
    $('.menu-mobile').removeClass('_active')
    $('.js-btn-menu').removeClass('_active')
    $('body').removeClass('bgActive')
    localStorage.setItem('animation-fps', false)
    $('.header__fps').removeClass('animation').removeClass('animation-fps')
    return false
}) 
$(document).on('click', function(event){
    if( $(event.target).closest('.search').length) 
    return; 
    $('.js-fps-btn').removeClass('_active');   
    event.stopPropagation();
});

if(!localStorage.getItem('animation-fps')){
    setTimeout(function(){
        $('.header__fps').removeClass('animation-fps');
        setInterval(function(){
            $('.header__fps').addClass('animation-fps');
            setTimeout(function(){
                $('.header__fps').removeClass('animation-fps');
            }, 1000)
        }, 6000)
    }, 1000)
}else {
    $('.header__fps').removeClass('animation').removeClass('animation-fps')
}


$.masked = function () {
    var $masked = $('.mask');
    $masked.mask("+7 (999) 999-99-99");
}
$.masked()
$(document).on('click', '.js-output', function(event){
  $(this).closest('.detalic__equipment-item').addClass('_active').siblings('.detalic__equipment-item').removeClass('_active');
  $('.card__info').remove();
  $('.card__prev').show();
  $('body').addClass('_active-mob')
  event.preventDefault()
  var link = $(this).attr('href');
  $.ajax({
      url: link,
      success: function(data) {
          $('.card__prev').hide();
              
          $('.card').addClass('load').addClass('open').append(data)
      },
      error: function(){
        $('.card').removeClass('load').removeClass('open')
      }
  });
})

$(document).on('click', '.js-output-close', function(event){
    event.stopPropagation();
    $('.detalic__equipment-item').removeClass('_active')
    $('body').removeClass('_active-mob')
    $('.card').removeClass('open')   
    return false
});

$('.js-output').hover(function(){
    $(this).siblings('.js-output').addClass('_hover')
  },function(){
    $(this).siblings('.js-output').removeClass('_hover')

  }
)
$(document).on('click', '._active-mob .bgDark',function(){
    $('body').removeClass('_active-mob')
    $('.card').removeClass('open') 
    $('.detalic__equipment-item').removeClass('_active')  
    event.stopPropagation();
})
if(ff == true){
    jQuery('.js-scroll').scrollbar();
}
$(document).on('click', '.js-search-btn', function(){
    $(this).toggleClass('_active');
    $('.menu-mobile').removeClass('_active')
    $('.js-btn-menu').removeClass('_active')
    $('body').removeClass('bgActive')
    $('.js-fps-btn').removeClass('_active')
    setTimeout(function(){
        $('.search__input').focus();
    },100)
    return false
})
$(document).on('change keyup', '.search__input', function(){

    if($(this).val().length) {
        $(this).next('.search__btn').prop('disabled', false);
    }else {
        $(this).next('.search__btn').prop('disabled', true);
    };
    return false
})
$(document).on('click', function(event){
    if( $(event.target).closest('.search').length) 
    return; 
    $('.js-search-btn').removeClass('_active');   
    event.stopPropagation();
});
$(document).on('click', '.js-search__close', function(event){
    $('.js-search-btn').removeClass('_active'); 
    $('.header__fps').removeClass('_active'); 
    return false  
});

if($('.no-result').length){
    $('.js-search-btn').addClass('_active')
}
$(document).on('click', '.tabs__link', function(){
    $(this).closest('li').addClass('_current').siblings('li').removeClass('_current')
    $(this).closest('.tabs').find('.tabs__box').eq($(this).closest('li').index()).show().siblings('.tabs__box').hide();
    optionLink = $(this).closest('li').index();
    $('.tabs-select').val(optionLink).trigger("change");
    return false
});
$('.tabs__list').after('<select class="select-box tabs-select"/>')

$('.tabs__li').each(function(){
    var optionSelect = $(this).text();
    var optionLink = $(this).index();
    if($(this).hasClass('_current')){
        $('.tabs-select').append('<option value="'+optionLink+'" selected>'+optionSelect+'</option>');
    }else {
        $('.tabs-select').append('<option value="'+optionLink+'">'+optionSelect+'</option>');
    }
})
$('.tabs-select').on('select2:select', function(e) {
    var data = e.params.data;
    $('.tabs__li').eq(data.id).addClass('_current').siblings('li').removeClass('_current')
    $(this).closest('.tabs').find('.tabs__box').eq(data.id).show().siblings('.tabs__box').hide();
});
  


$(document).on('click', '.tabs-ajax__link', function(){
    $(this).closest('li').addClass('_active').siblings('li').removeClass('_active');
    optionLink = $(this).closest('.tabs-ajax__li').index();
    var attrImg = $(this).attr('data-img');
    if (typeof attrImg !== typeof undefined && attrImg !== false) {
        $('.title-page__bg-img').attr('src', attrImg)
    }
    $('.tabs-ajax .tabs-ajax__select').val(optionLink).trigger("change");
});
$('.tabs-ajax__inner').append('<select class="select-box tabs-ajax__select"/>')

$(document).on('DOMContentLoaded', function(){
    $('.tabs-ajax__li').each(function(e) {
        var optionLink = $(this).closest('.tabs-ajax__li').index();
        $('.tabs-ajax .tabs-ajax__select').val(optionLink).trigger("change");
    })
})
$('.tabs-ajax__text').each(function(){
    var optionSelect = $(this).text(),
        optionLink = $(this).closest('.tabs-ajax__li').index();
    $('.tabs-ajax__select').append('<option value="'+optionLink+'">'+optionSelect+'</option>');
})
$('.tabs-ajax__select').on('select2:select', function (e) {
    var data = e.params.data;
    $('.tabs-ajax__li').eq(data.id).addClass('_active').siblings('li').removeClass('_active');    
    var link = $('.tabs-ajax__li').eq(data.id).find('.tabs-ajax__link').attr('href');   
    var attrImg = $('.tabs-ajax__li').eq(data.id).find('a').attr('data-img');
    if (typeof attrImg !== typeof undefined && attrImg !== false) {
        $('.title-page__bg-img').attr('src', attrImg)
    }
    location.href = link;
});
  
$(document).on('click', '.js-title-page', function(){
    $(this).closest('.title-page').toggleClass('_open');
    var open = $(this).data('open'),
        close = $(this).data('close');
    if($(this).closest('.title-page').hasClass('_open')){
        $(this).text(close)
        if($('.catalog__slider').length){
            setTimeout (function(){
                sliderCatalog();
            }, 100)
        }
    }else {
        $(this).text(open)
    }
    $(this).closest('.title-page').find('.title-page__hidd').slideToggle();
    return false
});
window.lazySizesConfig = {
	addClasses: true
};
$('.header').append('<div class="menu-mobile" style="--topm:167px;"><a href="#" class="menu-mobile__close"/><div class="menu-mobile__wrapper"/></div>');

$('body').prepend('<div class="bgDark"/>');

$('.menu-mobile__wrapper')
    .append($('.header__menu').clone())
    .append($('.header__menu-top').clone());

$(document).on('click', '.menu-mobile__close', function(){
    $(this).removeClass('_active');
    $('.menu-mobile').removeClass('_active');
    $('body').removeClass('bgActive');
    return false
});

$(document).on('click', '.bgDark', function(){
    $('.js-btn-menu').removeClass('_active');
    $('.menu-mobile').removeClass('_active');
    $('body').removeClass('bgActive');
    return false
});

$(document).on('click', '.js-btn-menu', function(){
    $(this).toggleClass('_active');
    $('.js-search-btn').removeClass('_active')
    $('.js-fps-btn').removeClass('_active')
    $('.menu-mobile').toggleClass('_active');
    $('body').toggleClass('bgActive');
    return false
});

$('.header__menu-top-hidd').each(function(){
    $(this).closest('.header__menu-top-box').addClass('two-lvl');
})

$(document).on('scroll', function(){
    var scr = 167 - $(window).scrollTop();
    if(scr < 70) {
        scr = 70;
    }
    $('.menu-mobile').css('--topm', scr) ;
    
})

$(document).on('click', '[data-modal]', function(e){
    let typeModal = $(this).attr('data-modal');
    e.preventDefault();
    var src = $(this).attr('href'), 
        bt = $(this),
        nameAmount,
        radioAmount,
        radioVal,
        radio,
        ValN,
        $this = $(this);

    $(document).on('click', '.js-modal-amount', function(e){
        e.stopPropagation();
        if($('.modal__amount-form').find('.radio__input').length){
            valChecked = $('.modal__amount-form').find('.radio__input:checked').val();
            $this.siblings('.amount__input').val(valChecked)
            $this.closest('.amount').siblings('.configurator__equipment-radio').find('.configurator__equipment-radio-input').attr('data-sum', valChecked)
            counterCall()
        }else {
            valChecked = $('.modal__amount-input').val();
            $this.siblings('.amount__input').val(valChecked)
            $this.closest('.amount').siblings('.configurator__equipment-radio').find('.configurator__equipment-radio-input').attr('data-sum', valChecked)
        }
        $.fancybox.close();
    });
    if($(this).hasClass('amount__mobile')){
        nameAmount = $(this).closest('.amount').attr('data-amoutnName');
        radioAmount = $(this).closest('.amount').attr('data-amountSum');
        radioAmount = JSON.parse(radioAmount);
        $('.modal__amount-name').empty().text(nameAmount);
        $('.modal__amount-form').empty();
        for(var i=0; i < radioAmount.length; i++){
            radioVal = radioAmount[i]
            $('.modal__amount-form').append('<div class="radio">'
                                                +'<label class="radio__label">'
                                                    +'<input type="radio" class="radio__input" name="amount" value="'+radioVal+'">'
                                                    +'<span class="radio__text">'
                                                        +radioVal
                                                    +'</span>'
                                                +'</label>'
                                            +'</div>');
        }
        $('.modal__amount-form').find('.radio__input:eq(0)').prop('checked', true);
    }
    if($(this).hasClass('amount__mobile-unit')){
        nameAmount = $(this).closest('.amount').attr('data-amoutnName');
        ValN = $(this).siblings('.amount__input').val();
        $('.modal__amount-name').empty().text(nameAmount);
        $('.modal__amount-form').empty();
        $('.modal__amount-form').append('<input type="text" class="modal__amount-input" value="'+ValN+'">');
    }
    $.fancybox.open({
        src: src,
        type: (typeModal !='ajax') ? 'inline' : 'ajax',
        hash: false,
        helpers: {
            overlay: {
                locked: false,
            }
        },
        opts: {
            smallBtn: true,
            btnTpl: {
               smallBtn: '<button data-fancybox-close class="fancybox-close-small modal__close close" title="{{CLOSE}}">' +
                    '<svg class="modal__close-icon">' +
                        '<use  xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="img/sprite.svg#icon-close1"></use>' +
                    '</svg>' +
                '</button>',                
                arrowLeft: '',
                arrowRight: '',
            },
            lang: "ru",
            i18n: {
                ru: {
                  CLOSE: "Закрыть",
                  NEXT: "Вперёд",
                  PREV: "Назад",
                  ERROR: "Запрашиваемый контент не был загружен <br/> Пожалуйста, попробуйте позднее.",
                  PLAY_START: "Запустить слайдшоу",
                  PLAY_STOP: "Остановить слайдшоу",
                  FULL_SCREEN: "На весь экран",
                  THUMBS: "Миниатюры",
                  DOWNLOAD: "Скачать",
                  SHARE: "Поделиться",
                  ZOOM: "Увеличить"
                }
            },
            infobar: false,
            arrows: false,
            zoom: false,
            buttons: [
                "zoom",
                "close"
            ],
            thumbs : {
                autoStart : true
            },
            touch: false,
            clickContent: false,    
            youtube : {
                controls : 0,
                showinfo : 0
            },
            beforeLoad: function(instance, current) {
                if (current.src == '#popup__search-main') {
                    $('.fancybox-container').addClass('fancybox__dark')
                }
                if (current.src == '#remove-account') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#card__question') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#reservation') {
                    $('.fancybox-container').addClass('fancybox__white-little')
                }
                if (current.src == '#popup__search-main') {
                    $('.fancybox-slide').addClass('event-no')
                }
            },
            afterShow: function() {
                $('.Valid').each(function(){
                    $.Valid($(this));
                });  
                $('.phone-masked').each(function(){
                    $.masked();
                }); 
                if($('.datepicker-input').length){
                    $.datepickerOp();
                };  
                $('.select').each(function(){
                    $.select($(this));
                });
                $('.multiple').each(function(){
                    $.selectMulte($(this));
                });
            },
            afterLoad: function(instance, current) {
                current.$content.closest('.fancybox-inner').addClass('fancy-text');
            },
        }
    });   
    return false 
});


$(document).on('click', '.search-page__tabs-link', function(){
    $(this).closest('li').addClass('_active').siblings('li').removeClass('_active');
    optionLink = $(this).closest('.search-page__tabs-li').index();
    $('.search-page__tabs .search-page__select').val(optionLink).trigger("change");
});
$('.search-page__inner').append('<select class="select-box search-page__select"/>')

$('.search-page__tabs-text').each(function(){
    var optionSelect = $(this).text(),
        optionLink = $(this).closest('.search-page__tabs-li').index();
    $('.search-page__select').append('<option value="'+optionLink+'">'+optionSelect+'</option>');
})
$('.search-page__select').on('select2:select', function (e) {
    var data = e.params.data;
    $('.search-page__tabs-li').eq(data.id).addClass('_active').siblings('li').removeClass('_active');    
    var link = $('.search-page__tabs-li').eq(data.id).find('.search-page__tabs-link').attr('href');   
    location.href = link;
});
  
$.select = function(el) {
    var $el = el;

    if (!$el.length) return;
    var dropdownParent = $(document.body);
    if ($el.parents('.select-box__conteiner').length !== 0)
      dropdownParent = $el.parents('.select-box__conteiner');
    
    var placeholder = $el.data('placeholder'); 
    $el.select2({
        placeholder: function(){
            $(this).data('placeholder');
        },
        allowClear: false,
        minimumResultsForSearch: -1,
        dropdownAutoWidth : true,
        dropdownParent: dropdownParent
    });
}
$('.select__game').on('select2:select', function (e) {
    var imgName = $(this.selectedOptions[0]).data('img');
    $('.'+imgName).removeClass('title-page__bg--hidd').siblings('.title-page__bg').addClass('title-page__bg--hidd')
});
$('.select-box').on('select2:open', function (e) {
    const block = document.querySelector('.select2-results__options');
    const block1 = document.querySelector('.select2-dropdown');
    const hasVerScroll = block1.scrollWidth - block.scrollWidth;
    if(hasVerScroll == 2 || hasVerScroll == 10){
        $('.select2-dropdown--below').addClass('_scroll')
    }
});

$.select($('.select-box'))



    
var swiperMain = new Swiper('.main-slider__container', {
    loop: true,
    navigation: {
        nextEl: '.main-slider__button--next',
        prevEl: '.main-slider__button--prev',
    },
    autoplay: {
        delay: 5000,
    },
    speed: 600,
    pagination: {
        el: '.main-slider__controll',
    }
});
swiperMain.on('slideChange', function (realIndex) {
    var $this = this.$el[0].parentElement
    if(!$this.classList.contains('delay')){
        $this.classList.add('delay')
    }
})

$(".main-slider").hover(function(){
    $(this).addClass('stop')
    swiperMain.autoplay.stop();
}, function(){
    $(this).removeClass('stop')
    swiperMain.autoplay.start();
});

var swiperDetalic = new Swiper('.detalic__slider-container', {
    loop: true,
    centeredSlides: true,
    speed: 600,
    pagination: {
        el: '.detalic__slider-controll',
    }
});

function sliderCatalog() {
    var swiperCatalog = new Swiper('.catalog__slider', {
        loop: true,
        freeMode: false,
        speed: 600,
        navigation: {
            nextEl: '.catalog__slider-button--next',
            prevEl: '.catalog__slider-button--prev',
        },
        pagination: {
            el: '.catalog__slider-controll',
            dynamicBullets: true,
            // dynamicMainBullets: 10,
        },
        breakpoints: {
            0: {
                slidesPerView: 'auto',
                spaceBetween: 16,
            },
            580: {
                slidesPerView: 'auto',
                spaceBetween: 24,
            },
            1024: {
                slidesPerView: 3,
                spaceBetween: 40,
            },
            1224: {
                slidesPerView: 4,
                spaceBetween: 40,
            },
        }
    });
    $(".catalog__slider").hover(function(){
        $(this).addClass('stop')
        swiperCatalog.autoplay.stop();
    }, function(){
        $(this).removeClass('stop')
        swiperCatalog.autoplay.start();
    });
}
// sliderCatalog();
function speedometer(el){
    el.each(function(){
        //Расчет спидометра и стрелок
        var testimonyMax = ($(this).data('max')),
            testimonyMaxRez = testimonyMax /100,
            testimonyReal = $(this).attr('data-real'),
            selectGame = $('.select__game').val(),
            selectResponsive = $('.select__responsive').val();
            testimonyReal = JSON.parse(testimonyReal);
            var objName = testimonyReal[selectGame],
                obgResp = objName[selectResponsive];
            if(testimonyMax < obgResp) {
                obgResp = testimonyMax;
            }
        var testimonyRealRez = obgResp / testimonyMaxRez,
            spadometrMax =  868,
            spadometrMin = 1000,
            spadometrPersent = 1.32,
            spadometrPosition = spadometrMin - (testimonyRealRez * 1.32),
            arrowMax = 0,
            arrowMin = -288,
            arrowPersent = -2.88,
            arrowPosition = arrowMin - (arrowPersent * testimonyRealRez);
        
        //цифры
        var count = $(this).find('.speedometer__counter').text(),
            block = $(this).find('.speedometer__counter'),
            countSpeed = obgResp > count ? (obgResp - count)*7 : (count - obgResp)*7,
            interval = setInterval(function(){
                if(count != obgResp){
                    if(count < obgResp){
                        count++;
                    }else if(count > obgResp){
                        count--;
                    }
                    block.text(count);
                    if(count == obgResp){
                        clearInterval(interval);
                    }
                }
            },7);
        block.text(count);
        $(this).find('.speedometer__arrow').css({'--speed': countSpeed+'ms', '--rotate': arrowPosition+'deg'})
        $(this).find('.speedometer__icon').css({'--stroke': spadometrPosition, '--speed': countSpeed+'ms'})
    })
};
$(document).on('change', 'select', function(){
    speedometer($('.speedometer.on'))
})

//анимация при скролле

var wow = new WOW(
    {
        boxClass: 'speedometer',      
        animateClass: 'on', 
        offset: $(window).width() > 767 ? 10: 100,          
        mobile: true,       
        live: true,       
        callback: function(box) {
            speedometer($('.speedometer.on'))
        },
        scrollContainer: null,    
        resetAnimation: true,     
    }
  );
  wow.init();
//validator
$.Valid = function (el) {

    var $el = el;

    if (!$el.length) return;
    var validator = $el.validate({
        rules: {
            name: {
                required: true
            },
            assembly: {
                required: true
            },
            secondname: {
                required: true
            },
            password: {
                required: true
            },
            password1: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            password2: {
                required: true,
                minlength: 6,
                equalTo: '[name="password1"]'
            },
            tel: {
                required: true,
                checkMask: true
            },
            telMail: {
                required: true,
                checkTelMail: true
            },
            check: {
                required: true
            },
            size: {
                required: true
            },
            city: {
                required: true
            },
            email: {
                required: true,
                mailVal: true
            },
            submitHandler: function (form) {
            }
        }
    })
    $.validator.addMethod("pwcheck", function(value) {
       return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) 
           && /[a-z]/.test(value) 
           && /\d/.test(value) 
    }, "Incorrect Password!");
    $.validator.addMethod("checkMask", function(value) {
        return /\+\d{1}[" "]\(\d{3}\)[" "]\d{3}-\d{2}-\d{2}/g.test(value); 
    });
     $.validator.addMethod("checkTelMail", function(value) {
         return /\+[0-9]{1,4}[0-9]{1,10}|(.*)@(.*)\.[a-z]{2,5}/g.test(value); 
    });
    $.validator.addMethod("mailVal", function(value) {
        return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value) 
     }, "Incorrect Password!");
};
$('.Valid').each(function(){
    $.Valid($(this));
});

 